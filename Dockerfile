FROM node:latest
RUN apt-get update -qy
RUN  apt-get install -y lftp

WORKDIR /usr/src/app/gitlab_cicd
COPY package.json ./
RUN npm install
COPY . .
EXPOSE 8080
CMD [ "node", "server.js" ]